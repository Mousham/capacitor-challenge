import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.midas.newapp',
  appName: 'my-capacitor-challenge',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
