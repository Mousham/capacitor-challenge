import { Component } from '@angular/core';
import { GoogleAuth } from '@codetrix-studio/capacitor-google-auth';
import { Plugins } from '@capacitor/core';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  userInfo = null;
  constructor(public alertController: AlertController) {}
//   ionViewDidEnter() {
//     GoogleAuth.init();

// }
async presentAlert() {
  const alert = await this.alertController.create({
    header: 'Alert',
    subHeader: 'Subtitle',
    message: 'This is an alert message.',
    buttons: ['OK']
  });

  await alert.present();
}
async googleSignup() {
  const googleUser = await Plugins.GoogleAuth.signIn(null) as any;
  console.log('my user: ', googleUser);
  this.userInfo = googleUser;
}
async doLogin() {
//   const user = await GoogleAuth.signIn();
//   if (user) { this.goToHome(user); }
// }
}
}
